# generic

#from django.db.models import Q
from rest_framework import viewsets
from django.shortcuts import render
from Employee_Registration.models import EmployeeRenewal
#from .permissions import IsOwnerOrReadOnly
from .serializers import EmployeeRenewalSerializer


class EmployeeRenewalView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = EmployeeRenewal.objects.all()
   serializer_class = EmployeeRenewalSerializer     

# class EmployeenameView(viewsets.ModelViewSet):
# 	queryset = Employeename.objects.all()
# 	serializer_class = EmployeenameSerializer
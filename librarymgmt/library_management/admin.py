from django.contrib import admin


from .models import BookDetail
from .models import BookRegistration

class BookDetailAdmin(admin.ModelAdmin):
	list_display = ('Types', 'BookName', 'Author',  'Publication', 'Published')
		
admin.site.register(BookDetail, BookDetailAdmin)

class BookRegistrationAdmin(admin.ModelAdmin):
	list_display = ('BookId', 'BookName')

admin.site.register(BookRegistration, BookRegistrationAdmin)
from django.conf import settings
from django.db import models
from django.urls import reverse

from rest_framework.reverse import reverse as api_reverse

# django hosts --> subdomain for reverse

class BookDetail(models.Model):
    # pk aka id --> numbers
    BOOK_CHOICES = (
        ('Technical','Technical'),
        ('Non-Technical','Non-Technical')
    )
    Types       = models.CharField(max_length = 100, choices = BOOK_CHOICES)
    BookName    = models.CharField(max_length=120, null=True, blank=True)
    Author      = models.CharField(max_length=120, null=True, blank=True)
    Publication = models.CharField(max_length=120, null=True, blank=True)
    Published   = models.CharField(max_length=120, null=True, blank=True)
    def __str__(self):
        return self.BookName

class BookRegistration(models.Model):
    BookId      = models.CharField(max_length=120, primary_key=True)
    BookName    = models.ForeignKey(BookDetail, related_name="custom_BookId_profile", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.BookId


   # @property
    #def owner(self):
     #   return self.types

    # def get_absolute_url(self):
    #     return reverse("api-postings:post-rud", kwargs={'pk': self.pk}) '/api/postings/1/'
    
    #def get_api_url(self, request=None):
     #   return api_reverse("api-postings:post-rud", kwargs={'pk': self.pk}, request=request)
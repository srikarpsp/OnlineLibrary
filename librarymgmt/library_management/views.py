
# Create your views here.
# generic

#from django.db.models import Q
from rest_framework import viewsets
from django.shortcuts import render
from .models import BookDetail, BookRegistration
#from .permissions import IsOwnerOrReadOnly
from .serializers import BookDetailSerializer, BookRegistrationSerializer



class BookDetailView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = BookDetail.objects.all()
   serializer_class = BookDetailSerializer     

class BookRegistrationView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = BookRegistration.objects.all()
   serializer_class = BookRegistrationSerializer


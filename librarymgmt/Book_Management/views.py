# generic

#from django.db.models import Q
from rest_framework import viewsets
from django.shortcuts import render
from Book_Management.models import Assignbook, Bookdetails
from library_management.models import BookDetail, BookRegistration
from Employee_Registration.models import EmployeeRenewal, EmployeeDetail
#from .permissions import IsOwnerOrReadOnly
from .serializers import AssignbookSerializer, BookDetailSerializer, EmployeeRenewalSerializer, BookdetailsSerializer


class AssignbookView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = Assignbook.objects.all()
   serializer_class = AssignbookSerializer     

class EmployeeRenewalView(viewsets.ModelViewSet):
	queryset = EmployeeRenewal.objects.all()
	serializer_class = EmployeeRenewalSerializer

class BookDetailView(viewsets.ModelViewSet):
	queryset = BookDetail.objects.all()
	serializer_class = BookDetailSerializer

class BookdetailsView(viewsets.ModelViewSet):
	queryset = Bookdetails.objects.all()
	serializer_class = BookdetailsSerializer





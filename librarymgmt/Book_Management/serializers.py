from rest_framework import serializers

from Book_Management.models import Assignbook, Bookdetails
from library_management.models import BookDetail, BookRegistration
from Employee_Registration.models import EmployeeRenewal, EmployeeDetail

class AssignbookSerializer(serializers.HyperlinkedModelSerializer): 
    class Meta:
        model = Assignbook
        fields = ('id', 'url', 'Employeeid', 'employeename', 'BookName', 'issue', 'renew')
        
class BookDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BookDetail
        fields = ('id', 'url', 'BookName')

class EmployeeRenewalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EmployeeRenewal
        fields = ('id', 'url', 'employeename')

class BookdetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EmployeeRenewal
        fields = ('id', 'url', 'employeename', 'assignbook')    
    


        #'timestamp','url',
        # read_only_fields = ['id', 'user']
'''
    # converts to JSON
    # validations for data passed

    def get_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_postings_url(request=request)

    def validate_title(self, value):
        qs = EmployeePost.objects.filter(title__iexact=value) # including instance
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
        if qs.exists():
            raise serializers.ValidationError("This title has already been used")
        return value
'''